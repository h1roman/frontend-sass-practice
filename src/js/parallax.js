const leftPhone = document.querySelectorAll('.leftPhoneParallax');
const rightPhone = document.querySelectorAll('.rightPhoneParallax');
const leftImages = document.querySelectorAll('.leftParallax');
const rightImages = document.querySelectorAll('.rightParallax');

document.addEventListener('scroll', function(){
    scrollPosition = window.pageYOffset;
    console.log(scrollPosition);
    leftPhone.forEach((i)=>{
        i.setAttribute('style', `transform: translate(${scrollPosition * -0.35}px,${scrollPosition * -0.5}px);`)
    });
    rightPhone.forEach((i)=>{
        i.setAttribute('style', `transform: translate(${scrollPosition * 0.35}px,${scrollPosition * -0.5}px);`)
    });
    leftImages.forEach((i)=>{
        i.setAttribute('style', `transform: translate(${scrollPosition * -0.75}px,${scrollPosition * -1}px);`)
    });
    rightImages.forEach((i)=>{
        i.setAttribute('style', `transform: translate(${scrollPosition * 0.75}px,${scrollPosition * -1}px);`)
    });
  });